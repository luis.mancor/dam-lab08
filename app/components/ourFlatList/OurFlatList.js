import React from 'react';
import {View, FlatList, StyleSheet, Text, ScrollView} from 'react-native';

const DATA = [
  {
    id: '1',
    title: 'First Item',
    description: 'First ',
  },
  {
    id: '2',
    title: 'Second Item',
    description: 'Second ',
  },
  {
    id: '3',
    title: 'Third Item',
    description: 'Third ',
  },

  {
    id: '1',
    title: 'First Item',
    description: 'First ',
  },
  {
    id: '2',
    title: 'Second Item',
    description: 'Second ',
  },
  {
    id: '3',
    title: 'Third Item',
    description: 'Third ',
  },
  {
    id: '1',
    title: 'First Item',
    description: 'First ',
  },
  {
    id: '2',
    title: 'Second Item',
    description: 'Second ',
  },
  {
    id: '3',
    title: 'Third Item',
    description: 'Third ',
  },
];

function Item({title}) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default function OurFlatList() {
  return (
    <View style={styles.container}>
      {/*<FlatList
        data={DATA}
        renderItem={({item}) => <Item title={item.description} />}
        keyExtractor={item => item.id}
      />*/}
      <ScrollView>
        {DATA.map(item => (
          <View style={styles.item}>
            <View style={{backgroundColor: 'blue', flex: 0.3}}>
              <Text style={styles.title}>{item.description}</Text>
            </View>
            <View style={{flex: 0.7}}>
              <Text style={[styles.title, {color: 'red', textAlign: 'center'}]}>
                {item.description}
              </Text>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //marginTop: 20,
    backgroundColor: 'green',
  },
  item: {
    flexDirection: 'row',
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 15,
    marginHorizontal: 30,
  },
  title: {
    fontSize: 32,
  },
});
