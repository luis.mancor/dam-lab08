var moment = require('moment');
moment.locale('os');
//require link react-native link @react-native-community/datetimepicker
import React, {Component} from 'react';
import ModalSelector from 'react-native-modal-selector';
import {Text, View, Button, StyleSheet} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { TextInput, NativeViewGestureHandler, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Switch } from 'react-native-paper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'; //yarn add react-native-keyboard-aware-scroll-view



export default class TransferenceFirst extends Component{
    constructor(props){
        super(props);
        let now = moment().format("DD/MM/YYYY");

        this.state={
            importe: 0,
            ctaOrigen:'',
            ctaDestino:'',
            ref:'',
            sendMail: '',
            accounts:'',
            fecha: now,
            isDatePickerVisible: false,
            refError:'',
            impError:'',
            ctaOrigenError:'',
            ctaDestinoError:'',
        };
    }

    changeImporte = importe =>{
        this.setState({importe});
    };
    changeCtaOrigen = ctaOrigen =>{
        this.setState({ctaOrigen});
    };
    changeCtaDestino = ctaDestino =>{
        this.setState({ctaDestino});
    };
    changeRef = ref =>{
        this.setState({ref});
    };

    refValidator(){
        if(this.state.ref == ""){
            this.setState({refError:"Campo necesario."})
        }else{
            this.setState({refError:""})
        }
    }

    impValidator(){
        if(this.state.importe == ""){
            this.setState({impError:"Solo se aceptan numeros."})
        }else{
            this.setState({impError:""})
        }
    }

    submit(){
        let rjx = /^[a-zA-Z]+$/;
        let numberrjx = /^-?[0-9][0-9,\.]+$/;
        let isValid= rjx.test(this.state.ref)
        let isValidImporte = numberrjx.test(this.state.importe)
        let isValidCtaOrigen = numberrjx.test(this.state.ctaOrigen)
        let isValidCtaFin    = numberrjx.test(this.state.ctaDestino)

        console.warn(isValid)
        if(!isValid ){  //|| !isValidImporte
            this.setState({refError: "Campo obligatorio"})
        }if(!isValidImporte){
            this.setState({impError: "Solo se aceptan numeros"})
        }if(!isValidCtaOrigen){
            this.setState({ctaOrigenError: "Campo obligatorio"})
        }if(!isValidCtaFin){
            this.setState({ctaDestinoError: "Campo obligatorio"})
        }else{
            this.props.navigation.navigate('Informacion' ,{
                cuentaOrigen: this.state.ctaOrigen,
                cuentaDestino: this.state.ctaDestino,
                importe: this.state.importe,
                ref: this.state.ref,
                fecha: this.state.fecha,
                })
        }
    }

    render(){

        const cuenta = [
            { key: 1, label: '0000000001234' },
            { key: 2, label: '0000000001235' },
            { key: 3, label: '0000000001236' },
            { key: 4, label: '0000000001237'}
        ];
        
        const showDatePicker = () => {
            this.setState({isDatePickerVisible:true});
        };
         
        const hideDatePicker = () => {
            this.setState({isDatePickerVisible:false});
        };
         
        const handleConfirm = (date) => {
            let fecNew = moment(date).format("DD/MM/YYYY");
        this.setState({fecha: fecNew, isDatePickerVisible:false});
        };

        return(
            <View style={styles.container}>
                <KeyboardAwareScrollView>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginBottom: 5,
                    }}
                    >
                    <View   style={{flexDirection: 'column', alignSelf: 'stretch'}}>
                        <Text style={styles.titleInput}>Cuenta origen</Text>
                        <View style={[styles.pickerModal, {height:35}]}>
                            <ModalSelector
                                data={cuenta}
                                initValue="Seleccione una cuenta"
                                accessible={true}
                                onChange={(option)=>{ this.setState({ctaOrigen:option.label})}}>
                                <TextInput
                                    style={[styles.picker, {height: 40, borderRadius: 6}]}
                                    editable={false}
                                    value={this.state.ctaOrigen} />
                            </ModalSelector>
                        </View>
                        <Text style={{color:'red'}}>{this.state.ctaOrigenError}</Text>
                    </View>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginBottom: 5,
                    }}
                    >
                    <View   style={{flexDirection: 'column', alignSelf: 'stretch'}}>
                        <Text style={styles.titleInput}>Cuenta destino</Text>
                        <View style={[styles.viewInput, {height:35}]}>
                            <ModalSelector
                                data={cuenta}
                                initValue="Seleccione una cuenta"
                                accessible={true}
                                onChange={(option)=>{ this.setState({ctaDestino:option.label})}}>
                                <TextInput
                                    style={[styles.picker, {height: 40, borderRadius: 6}]}
                                    editable={false}
                                    value={this.state.ctaDestino} />
                            </ModalSelector>
                           
                        </View>
                        <Text style={{color:'red'}}>{this.state.ctaDestinoError}</Text>
                    </View>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginBottom: 5,
                    }}
                    >
                    <View   style={{flexDirection: 'column', alignSelf: 'stretch'}}>
                        <Text style={styles.titleInput}>Importe</Text>
                        <View style={[styles.viewInput, {height:35}]}>
                            <TextInput
                                onBlur={() => this.impValidator()}
                                keyboardType="numeric"
                                underlineColorAndroid={'transparent'}
                                style={[styles.picker, {height: 40, borderRadius: 6}]}
                                onChangeText={importe => this.changeImporte(importe)}
                            />
                        </View>
                        <Text style={{color:'red'}}>{this.state.impError}</Text>
                        
                    </View>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginBottom: 5,
                    }}
                    >
                    <View   style={{flexDirection: 'column', alignSelf: 'stretch'}}>
                        <Text style={styles.titleInput}>Referencia</Text>
                        <View style={[styles.viewInput, {height:35}]}>
                            <TextInput
                                onBlur={() => this.refValidator()}
                                underlineColorAndroid={'transparent'}
                                style={[styles.picker, {height: 40, borderRadius: 6}]}      
                                onChangeText={ref => this.changeRef(ref)}
                            />
                        </View>
                        <Text style={{color:'red'}}>{this.state.refError}</Text>
                    </View>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginBottom: 5,
                    }}
                    >
                    <View   style={{alignSelf: 'center', justifyContent:'center' , alignContent:'center', alignItems:'center'}}>
                        <Text style={styles.titleInput}>Fecha</Text>
                        <View style={[styles.viewInput, {height:35}]}>
                            <TouchableWithoutFeedback onPress={showDatePicker}>
                                <View style={styles.viewCalendar}>
                                    <Text style={styles.title}>
                                        {this.state.fecha}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <DateTimePickerModal
                                isVisible={this.state.isDatePickerVisible}
                                mode="date"
                                onConfirm={handleConfirm}
                                onCancel={hideDatePicker}
                            />
                        </View>
                    </View>
                </View>


                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginTop: 5,
                    }}>
                    <Text
                        style={[
                            styles.titleInput,
                            {marginBottom: 0, marginTop: 4, marginRight:5},
                        ]}
                        >
                        Notificarme al mail
                    </Text>
                    <Switch
                        onValueChange={value => this.setState({sendMail: value})}
                        onTintColor={'#063984'}
                        thumbTintColor={'#e2e2e2'}
                        tintColor="#cdcdcd"
                        value={this.state.sendMail}
                    />
                </View>

                <Button title="Aceptar" 
                
                onPress={() => {this.submit()}
                } 
                />
                                    
                </KeyboardAwareScrollView>
            </View>
        )
    }

    
}

const styles = StyleSheet.create({
    titleInput: {
      //flex: 1,
      //marginTop: 20,
      //backgroundColor: 'green',
    },
    viewInput:{
    },
    picker:{
        backgroundColor: '#DCCCDB',
        width: 480,

    },
    pickerModal:{

    },
    viewCalendar:{
        backgroundColor: '#DCCCDB',
        borderColor: 'black',
        borderRadius:20,
    },
    container:{
        margin: 15,
        //backgroundColor:'#DDDDDD',
    },

    Contenedor: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        justifyContent: 'flex-start',
        margin: 15,
    },
  });
  


/*


                                            <Button title="Go Next" 
                
                onPress={() => this.props.navigation.navigate('Second' ,{
                    cuentaOrigen: this.state.ctaOrigen,
                    cuentaDestino: this.state.ctaDestino,
                    importe: this.state.importe,
                    ref: this.state.ref,
                    fecha: this.state.fecha,
                    })
                } 
                />
*/