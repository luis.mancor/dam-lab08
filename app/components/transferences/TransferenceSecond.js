var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button, StyleSheet} from 'react-native';
import { BaseRouter } from '@react-navigation/native';

export default class TransferenceSecond extends Component{
    constructor(props){
        super(props)
        this.state={
            id: this.props.route.params.importe,
            ctaOrigen: this.props.route.params.cuentaOrigen,
        };
    }

    render(){

        const { cuentaOrigen, cuentaDestino, importe,ref, fecha } = this.props.route.params
        return(
            <View style={styles.container}>
                <Text>Cuenta de Origen</Text>
                <View style={[styles.viewInput, {height:22}]}>
                    <Text>{this.state.ctaOrigen}</Text>
                </View>

                <Text>Cuenta de destino</Text>
                <View style={[styles.viewInput, {height:22}]}>
                    <Text>{cuentaDestino}</Text>
                </View>
                
                <Text>Importe</Text>
                <View style={[styles.viewInput, {height:22}]}>
                    <Text>{this.state.id}</Text>                  
                </View>

                <Text>Referencia</Text>
                <View style={[styles.viewInput, {height:22}]}>
                    <Text>{ref}</Text>                   
                </View>

                <Text>Fecha</Text>
                <View style={[styles.viewInput, {height:22}]}>
                    <Text>{fecha}</Text>
                </View>

                <View style={{ flexDirection:"row", alignSelf:'center' }}>
                    <View style={styles.buttonStyle}>
                        <Button title="Regresar" onPress={() => this.props.navigation.navigate('Datos generales')} />
                    </View>
                    <View style={styles.buttonStyle}>
                        <Button title="Confirmar" onPress={() => this.props.navigation.navigate('Completado')} />
                    </View>
                </View>
            </View>
        )
    }


}

function getParametros({route, navigation}) {
    const { cuentaOrigen } = route.params;
    return (
        <View>
            <Text>{JSON.stringify(cuentaOrigen)}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    titleInput: {
      //flex: 1,
      //marginTop: 20,
      //backgroundColor: 'green',
    },
    viewInput:{
        backgroundColor: '#CCE1D5',
    },
    picker:{
        backgroundColor: '#DCCCDB',
        width: 480,

    },
    pickerModal:{

    },
    viewCalendar:{
    },
    container:{
        margin: 15,
        //backgroundColor:'#DDDDDD',
    },
    buttonStyle:{
        justifyContent: 'center',
        alignItems:'center',
        alignSelf:'center',
        margin:30,
    },
  });
  